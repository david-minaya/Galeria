var visor;
var agregarVisor = true;
var imagenes;
var viñeta;
var imagen;
var titulo;
var autor;
var options;
var categoria = "*";
var tabla;
var alertDialogAgregarImagen;
var seleccionarImagen;
var preview;
var inputTitulo; 
var inputAutor;
var inputCategoria;
var inputUrl;
var submit;

window.onload = function() {
    
    tabla = document.querySelector("#table");
    visor = document.querySelector("#visor");
    imagen = document.querySelector("#visor #imagen");
    titulo = document.querySelector("#titulo");
    autor = document.querySelector("#autor");
    options = document.querySelector("#filtro");
    var agregar = document.querySelector("#boton-agregar");
    var eliminar = document.querySelector("#boton-eliminar");
    
    alertDialogAgregarImagen = document.querySelector("#alert-dialog-agregar-imagen");
    seleccionarImagen = document.querySelector("#alert-dialog-agregar-imagen #seleccionar-imagen");
    preview = document.querySelector("#preview");
    inputTitulo = document.querySelector("#agregar-informacion #titulo");
    inputAutor = document.querySelector("#agregar-informacion #autor");
    inputCategoria = document.querySelector("#agregar-informacion #categoria");
    inputUrl = document.querySelector("#agregar-informacion #url");
    submit = document.querySelector("#submit");

    imagenes = storeImagenes;

    crearTabla();

    document.querySelector("#div-imagen #cerrar").onclick = () => {
        agregarVisor = true;
        visor.style.display = "none";
    }

    options.onchange = (option) => {
        categoria = option.target.value;
        crearTabla();
    }
    agregar.onclick = () => alertDialogAgregarImagen.style.display = "block";
    eliminar.onclick = onclickBotonEliminar;
    
    document.querySelector("#alert-dialog-agregar-imagen #cerrar").onclick = (e) => {
        e.stopPropagation();
        clearForm();
    }
    seleccionarImagen.onclick = () => document.querySelector("#fileElement").click();
    seleccionarImagen.addEventListener("dragenter", dragenter, false);
    seleccionarImagen.addEventListener("dragover", dragover, false);
    seleccionarImagen.addEventListener("drop", drop, false);
    inputUrl.onchange = onChangeInputURL;
    submit.onclick = onClickSubmit;
}

function crearTabla() {

    // Filtra las imagenes
    if (categoria === "*") {
        imagenes = storeImagenes;

    } else {
        imagenes = storeImagenes.filter(img => img.category === categoria);
    }

    tabla.innerHTML = "";

    for(var i = 0; i < imagenes.length; i++) {
        
        var item = document.createElement("div");
        var img = document.createElement("img");
        var check = document.createElement("input");

        item.classList.add("item");
        
        img.src = imagenes[i].miniatura;
        img.alt = imagenes[i].title;
        img.onclick = onClickImg;

        check.type = "checkbox";
        check.classList.add("check");
        check.onchange = onChangeCkeck;

        item.append(check);
        item.append(img);

        tabla.append(item);
    }
}

function onChangeCkeck(event) {

    var checks = document.querySelectorAll(".check");
    var checksChecked = document.querySelectorAll(".check:checked");

    // Si no hay checkbox marcados los oculta, de lo contrario los
    // muestra.
    if (checksChecked.length < 1) {

        checks.forEach(check => {
            check.removeAttribute("style");
        })

    } else {

        checks.forEach(check => {
            check.style.visibility = "visible";
        })
    }
}

function onClickImg(event) {

    // Agrega el visor la primera vez que el usuario hace click sobre
    // una imagen.
    if(agregarVisor) {
        visor.style.display = 'block';
        imagen.style.display = 'block';
        agregarVisor = false;
    }

    imagen.removeAttribute("src");

    // Obtiene la imagen seleccionada por el usuario
    var currentImg = event.target;

    imagenes.forEach(i => {
        
        if (currentImg.src === i.miniatura) {
            imagen.src = i.img;
            titulo.innerText = i.title;
            titulo.href = i.url;
            titulo.target = "about-blank";
            autor.innerText = i.username;
            autor.href = "https://www.deviantart.com/" + i.username;
            autor.target = "about-blank";
        }
    });

}

function onclickBotonEliminar() {

    var items = document.querySelectorAll(".item");

    items.forEach(item => {
        
        var checkbox = item.querySelector(".check");

        if (checkbox.checked) {

            var imagen = item.querySelector("img");
            var src = imagen.src;

            storeImagenes.forEach((img, i, imgs) => {

                if (src == img.miniatura) {
                    imgs.splice(i, 1);
                }

            });
        }
    });

    crearTabla();
}

function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
}
  
function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
}

function drop(e) {
    e.stopPropagation();
    e.preventDefault();

    var files = e.dataTransfer.files;

    handleFiles(files);
}

function handleFiles(f) {

    var file = f[0];

    if (!file.type.startsWith("image/")) {

        alert("El archivo tiene que ser una imagen");

        return;
    }

    var url = window.URL.createObjectURL(file);

    preview.src = url;

    var inputTitulo = document.querySelector("#agregar-informacion #titulo");
    inputTitulo.value = file.name;

    var inputUrl = document.querySelector("#agregar-informacion #url");
    inputUrl.value = url;
}

function onChangeInputURL(e) {

    var index = e.target.value.lastIndexOf("/") + 1;
    var filename = e.target.value.substr(index);
    var inputTitulo = document.querySelector("#agregar-informacion #titulo");

    preview.src = e.target.value;
    inputTitulo.value = filename;
}

function onClickSubmit(event) {

    if (preview.src == "") {
        alert("Primero tienes que seleccionar una imagen");
        return;
    }

    var titulo = inputTitulo.value;
    var autor = inputAutor.value !== "" ? inputAutor.value : "none";
    var categoria = inputCategoria.value !== "" ? inputCategoria.value : "none";
    var url = inputUrl.value;

    var imagen = {
        url: url,
        title: titulo,
        category: categoria,
        username: autor,
        usericon: "",
        img: url,
        miniatura: url
    }

    // Agrega la imagen al principio de la lista
    storeImagenes.unshift(imagen);
    
    crearTabla();
    clearForm();

}

function clearForm() {

    preview.removeAttribute("src");
    inputTitulo.value = "";
    inputAutor.value = "";
    inputCategoria.value = "";
    inputUrl.value = "";

    alertDialogAgregarImagen.style.display = "none";
}

var storeImagenes = [
    {
        "url": "https://www.deviantart.com/cryptid-creations/art/Daily-Paint-2080-Furtune-Teller-757439329",
        "title": "Daily Paint #2080. Furtune Teller",
        "category": "Animals",
        "username": "Cryptid-Creations",
        "usericon": "https://a.deviantart.net/avatars/c/r/cryptid-creations.png",
        "img": "https://orig00.deviantart.net/a1c2/f/2018/213/9/7/daily_paint__2080__furtune_teller_by_cryptid_creations-dciyjxd.png",
        "miniatura": "https://t00.deviantart.net/icrodbELkAxNpSYUcc8qNyOzYpI=/300x200/filters:fixed_height(100,100):origin()/pre00/94a4/th/pre/f/2018/213/9/7/daily_paint__2080__furtune_teller_by_cryptid_creations-dciyjxd.png"
    },
    {
        "url": "https://www.deviantart.com/jaygraphixx/art/A-Glance-Of-Nature-757322178",
        "title": "A Glance Of Nature",
        "category": "Fantasy",
        "username": "JayGraphixx",
        "usericon": "https://a.deviantart.net/avatars/j/a/jaygraphixx.jpg?13",
        "img": "https://img00.deviantart.net/a15f/i/2018/213/a/b/a_glance_of_nature_by_jaygraphixx-dciw1j6.jpg",
        "miniatura": "https://t00.deviantart.net/4uQ5fxnHD9BawL5B-D3eKGznhY8=/300x200/filters:fixed_height(100,100):origin()/pre00/ec14/th/pre/i/2018/213/a/b/a_glance_of_nature_by_jaygraphixx-dciw1j6.jpg"
    },
    {
        "url": "https://www.deviantart.com/tazihound/art/First-Meeting-762719474",
        "title": "First Meeting",
        "category": "Animals",
        "username": "Tazihound",
        "usericon": "",
        "img": "https://orig00.deviantart.net/cce2/f/2018/248/1/2/first_meeting_by_tazihound-dcm3q42.jpg",
        "miniatura": "https://t00.deviantart.net/bJ3JKwdmfLuLtAWmVJqz7IOgqtU=/300x200/filters:fixed_height(100,100):origin()/pre00/3636/th/pre/f/2018/248/1/2/first_meeting_by_tazihound-dcm3q42.jpg"
    },
    {
        "url": "https://www.deviantart.com/harveytolibao/art/Ms-Marvel-Harvey-Kevin-Tolibao-757334358",
        "title": "Ms Marvel - Harvey | Kevin Tolibao",
        "category": "Other",
        "username": "harveytolibao",
        "usericon": "https://a.deviantart.net/avatars/h/a/harveytolibao.png?1",
        "img": "https://img00.deviantart.net/3f3e/i/2018/213/a/f/ms_marvel___harvey___kevin_tolibao_by_harveytolibao-dciwaxi.jpg",
        "miniatura": "https://t00.deviantart.net/OB1qSKo7RF6aV2qiebyucPXf8II=/300x200/filters:fixed_height(100,100):origin()/pre00/c2a3/th/pre/i/2018/213/a/f/ms_marvel___harvey___kevin_tolibao_by_harveytolibao-dciwaxi.jpg"
    },
    {
        "url": "https://www.deviantart.com/sunima/art/Wolf-cub-kiss-757375916",
        "title": "Wolf cub kiss",
        "category": "Animals",
        "username": "Sunima",
        "usericon": "https://a.deviantart.net/avatars/s/u/sunima.png?1",
        "img": "https://img00.deviantart.net/d0ad/i/2018/213/8/a/wolf_cub_kiss_by_sunima-dcix6zw.jpg",
        "miniatura": "https://t00.deviantart.net/hBJ6mKh8KnJuEYrrBumoEfg3F48=/300x200/filters:fixed_height(100,100):origin()/pre00/3bdd/th/pre/i/2018/213/8/a/wolf_cub_kiss_by_sunima-dcix6zw.jpg"
    },
    {
        "url": "https://www.deviantart.com/pajunen/art/Iron-Age-Village-757385112",
        "title": "Iron Age Village",
        "category": "Exterior",
        "username": "Pajunen",
        "usericon": "https://a.deviantart.net/avatars/p/a/pajunen.gif?2",
        "img": "https://orig00.deviantart.net/d0d1/f/2018/213/5/9/iron_age_village_by_pajunen-dcixe3c.jpg",
        "miniatura": "https://t00.deviantart.net/6xazpXrZbAKTN6D345EpbKrzqbQ=/300x200/filters:fixed_height(100,100):origin()/pre00/ae77/th/pre/f/2018/213/5/9/iron_age_village_by_pajunen-dcixe3c.jpg"
    },
    {
        "url": "https://www.deviantart.com/wilvarin-liadon/art/Twilight-Sparkle-757388109",
        "title": "Twilight Sparkle",
        "category": "Other",
        "username": "Wilvarin-Liadon",
        "usericon": "https://a.deviantart.net/avatars/w/i/wilvarin-liadon.png?7",
        "img": "https://orig00.deviantart.net/cdf8/f/2018/213/1/b/twilight_sparkle_by_wilvarin_liadon-dcixgel.gif",
        "miniatura": "https://t00.deviantart.net/2wD2bESvJ0DV1y3eaIHifui36RM=/300x200/filters:fixed_height(100,100):origin()/pre00/4678/th/pre/f/2018/213/e/4/twilight_sparkle_by_wilvarin_liadon-dcixgel.png"
    },
    {
        "url": "https://www.deviantart.com/2wenty/art/Alecti-Ward-The-Moon-s-Chosen-757378884",
        "title": "Alecti Ward, The Moon's Chosen",
        "category": "Fantasy",
        "username": "2wenty",
        "usericon": "https://a.deviantart.net/avatars/2/w/2wenty.jpg?2",
        "img": "https://img00.deviantart.net/7c89/i/2018/213/d/d/alecti_ward__the_moon_s_chosen_by_2wenty-dcix9ac.jpg",
        "miniatura": "https://t00.deviantart.net/CflFVlCjAGqmP7SOZNRWeVR5zHg=/300x200/filters:fixed_height(100,100):origin()/pre00/1fe2/th/pre/i/2018/213/d/d/alecti_ward__the_moon_s_chosen_by_2wenty-dcix9ac.jpg"
    },
    {
        "url": "https://www.deviantart.com/patrykgpl/art/Zapdos-Papercraft-757318414",
        "title": "Zapdos Papercraft",
        "category": "Models",
        "username": "PatrykGPL",
        "usericon": "https://a.deviantart.net/avatars/p/a/patrykgpl.jpg?2",
        "img": "https://img00.deviantart.net/8bd3/i/2018/213/8/4/zapdos_papercraft_by_patrykgpl-dcivymm.jpg",
        "miniatura": "https://t00.deviantart.net/cFSWrg3msOL-t4GxVj8B4e-aH44=/300x200/filters:fixed_height(100,100):origin()/pre00/c4f8/th/pre/i/2018/213/8/4/zapdos_papercraft_by_patrykgpl-dcivymm.jpg"
    },
    {
        "url": "https://www.deviantart.com/neko-rina/art/Video-Glossy-Miruku-726292458",
        "title": "[+Video] Glossy Miruku",
        "category": "Drawings",
        "username": "Neko-Rina",
        "usericon": "https://a.deviantart.net/avatars/n/e/neko-rina.gif?5",
        "img": "https://orig00.deviantart.net/9264/f/2018/213/9/2/927e28633bd0cc5acd92d41ff83482e1-dc0eyui.png",
        "miniatura": "https://t00.deviantart.net/HGT7MzMCk8VWOPWeEU4MuK_AjHk=/300x200/filters:fixed_height(100,100):origin()/pre00/30d2/th/pre/f/2018/213/9/2/927e28633bd0cc5acd92d41ff83482e1-dc0eyui.png"
    },
    {
        "url": "https://www.deviantart.com/pony-berserker/art/Replacement-Parts-757268296",
        "title": "Replacement Parts",
        "category": "Movies & TV",
        "username": "Pony-Berserker",
        "usericon": "https://a.deviantart.net/avatars/p/o/pony-berserker.png?5",
        "img": "https://img00.deviantart.net/fa80/i/2018/212/f/c/replacement_parts_by_pony_berserker-dciuvyg.png",
        "miniatura": "https://t00.deviantart.net/Ej-GHRoHYcy0v5N38VPUBVXZDCk=/300x200/filters:fixed_height(100,100):origin()/pre00/b3aa/th/pre/i/2018/212/f/c/replacement_parts_by_pony_berserker-dciuvyg.png"
    },
    {
        "url": "https://www.deviantart.com/whitekimya/art/Fishing-Lessons-757372313",
        "title": "Fishing Lessons",
        "category": "Movies & TV",
        "username": "WhiteKimya",
        "usericon": "https://a.deviantart.net/avatars/w/h/whitekimya.png?8",
        "img": "https://orig00.deviantart.net/548d/f/2018/213/e/6/e60633f1455faafd1905af36f5bd76dc-dcix47t.jpg",
        "miniatura": "https://t00.deviantart.net/ttV_Mb1Fg3I74neSXwUk2G5wxvc=/300x200/filters:fixed_height(100,100):origin()/pre00/90e4/th/pre/f/2018/213/e/6/e60633f1455faafd1905af36f5bd76dc-dcix47t.jpg"
    },
    {
        "url": "https://www.deviantart.com/jayaxer/art/True-Arcade-Goodness-757370548",
        "title": "True Arcade Goodness",
        "category": "Non-Isometric",
        "username": "JayAxer",
        "usericon": "https://a.deviantart.net/avatars/j/a/jayaxer.gif",
        "img": "https://orig00.deviantart.net/f5b6/f/2018/213/9/a/true_arcade_goodness_by_jayaxer-dcix2us.png",
        "miniatura": "https://t00.deviantart.net/jc4FmoqldAJqNfv2ZvYmgZ4abHk=/300x200/filters:fixed_height(100,100):origin()/pre00/24d0/th/pre/f/2018/213/9/a/true_arcade_goodness_by_jayaxer-dcix2us.png"
    },
    {
        "url": "https://www.deviantart.com/chateaugrief/art/Moaning-Caverns-757382037",
        "title": "Moaning Caverns",
        "category": "Landscapes & Scenery",
        "username": "chateaugrief",
        "usericon": "https://a.deviantart.net/avatars/c/h/chateaugrief.png?3",
        "img": "https://img00.deviantart.net/62a7/i/2018/213/8/b/moaning_caverns_by_chateaugrief-dcixbpx.jpg",
        "miniatura": "https://t00.deviantart.net/1MDW2GQkw9Dj47H6c5K6JXpLKvg=/300x200/filters:fixed_height(100,100):origin()/pre00/7ab6/th/pre/i/2018/213/8/b/moaning_caverns_by_chateaugrief-dcixbpx.jpg"
    },
    {
        "url": "https://www.deviantart.com/saphirenishi/art/saph-industries-757345920",
        "title": "saph industries",
        "category": "Female",
        "username": "SaphireNishi",
        "usericon": "https://a.deviantart.net/avatars/s/a/saphirenishi.png?4",
        "img": "https://orig00.deviantart.net/2946/f/2018/213/f/5/saph_industries_by_saphirenishi-dciwjuo.jpg",
        "miniatura": "https://t00.deviantart.net/Enx8Rx_rxPa4GCqViMy9xfyxdNQ=/300x200/filters:fixed_height(100,100):origin()/pre00/c21d/th/pre/f/2018/213/f/5/saph_industries_by_saphirenishi-dciwjuo.jpg"
    },
    {
        "url": "https://www.deviantart.com/mosternight-mmd/art/Hair-Edit-Pack-Dl-By-MosterNight-MMD-757387412",
        "title": "Hair Edit Pack Dl! // By-MosterNight-MMD",
        "category": "3D",
        "username": "MosterNighT-MMD",
        "usericon": "https://a.deviantart.net/avatars/m/o/mosternight-mmd.gif?4",
        "img": "https://orig00.deviantart.net/23dd/f/2018/213/f/0/hair_edit_pack_dl_____by_mosternight_mmd_by_mosternight_mmd-dcixfv8.png",
        "miniatura": "https://t00.deviantart.net/oWyxIqRRigzK7DOlM7ixWEJ8H-Y=/300x200/filters:fixed_height(100,100):origin()/pre00/48c8/th/pre/f/2018/213/f/0/hair_edit_pack_dl_____by_mosternight_mmd_by_mosternight_mmd-dcixfv8.png"
    },
    {
        "url": "https://www.deviantart.com/shilin/art/Evening-757130502",
        "title": "Evening",
        "category": "Drawings",
        "username": "shilin",
        "usericon": "https://a.deviantart.net/avatars/s/h/shilin.gif?1",
        "img": "https://orig00.deviantart.net/8af8/f/2018/211/5/f/evening_by_shilin-dcirxmu.png",
        "miniatura": "https://t00.deviantart.net/T8pEBMQ6jlxmx75uspbi35BoMKM=/300x200/filters:fixed_height(100,100):origin()/pre00/0cce/th/pre/f/2018/211/5/f/evening_by_shilin-dcirxmu.png"
    },
    {
        "url": "https://www.deviantart.com/zarla/art/Like-the-heart-of-a-slumbering-god-757427792",
        "title": "Like the heart of a slumbering god",
        "category": "Games",
        "username": "zarla",
        "usericon": "https://a.deviantart.net/avatars/z/a/zarla.gif",
        "img": "https://orig00.deviantart.net/a1d4/f/2018/213/2/c/like_the_heart_of_a_slumbering_god_by_zarla-dciyb0w.png",
        "miniatura": "https://t00.deviantart.net/d_evMLpDt0g_NCh4jtu89Cff_gw=/300x200/filters:fixed_height(100,100):origin()/pre00/b3d2/th/pre/f/2018/213/2/c/like_the_heart_of_a_slumbering_god_by_zarla-dciyb0w.png"
    },
    {
        "url": "https://www.deviantart.com/darkmechanic/art/Adamantium-Sky-757355428",
        "title": "Adamantium Sky",
        "category": "Sci-Fi",
        "username": "DarkMechanic",
        "usericon": "https://a.deviantart.net/avatars/d/a/darkmechanic.jpg?4",
        "img": "https://img00.deviantart.net/2349/i/2018/213/c/7/adamantium_sky_by_darkmechanic-dciwr6s.jpg",
        "miniatura": "https://t00.deviantart.net/5VO4yOe4bOrV34bs-Y0Xf_mp27I=/300x200/filters:fixed_height(100,100):origin()/pre00/41e2/th/pre/i/2018/213/c/7/adamantium_sky_by_darkmechanic-dciwr6s.jpg"
    },
    {
        "url": "https://www.deviantart.com/xeno-photography/art/Fate-Grand-Order-Jeanne-D-Arc-Alter-757335109",
        "title": "Fate/Grand Order - Jeanne D'Arc Alter",
        "category": "Cosplay",
        "username": "Xeno-Photography",
        "usericon": "https://a.deviantart.net/avatars/x/e/xeno-photography.jpg?3",
        "img": "https://img00.deviantart.net/27de/i/2018/213/b/b/fate_grand_order___jeanne_d_arc_alter_by_xeno_photography-dciwbid.jpg",
        "miniatura": "https://t00.deviantart.net/kTk8-yGsxIg-4yAiWL4NqTX3Rqo=/300x200/filters:fixed_height(100,100):origin()/pre00/f1cb/th/pre/i/2018/213/b/b/fate_grand_order___jeanne_d_arc_alter_by_xeno_photography-dciwbid.jpg"
    },
    {
        "url": "https://www.deviantart.com/howling-madfoxhatter/art/Odd-Karting-Crossover-757321703",
        "title": "Odd Karting Crossover",
        "category": "Games",
        "username": "HOwLing-MAdFoxHatter",
        "usericon": "https://a.deviantart.net/avatars/h/o/howling-madfoxhatter.png?1",
        "img": "https://img00.deviantart.net/2b5b/i/2018/213/a/4/odd_karting_crossover_by_howling_madfoxhatter-dciw15z.png",
        "miniatura": "https://t00.deviantart.net/_i2JWccySAW7xDGrvIY02Th3SO0=/300x200/filters:fixed_height(100,100):origin()/pre00/b816/th/pre/i/2018/213/a/4/odd_karting_crossover_by_howling_madfoxhatter-dciw15z.png"
    },
    {
        "url": "https://www.deviantart.com/go/journal/Ripley-s-Unconventional-Art-Contest-754841619",
        "title": "Sheith band au",
        "category": "Movies & TV",
        "username": "DJune-y",
        "usericon": "https://a.deviantart.net/avatars/d/j/djune-y.png?1",
        "img": "https://orig00.deviantart.net/2813/f/2018/213/7/e/sheith_band_au_by_djune_y-dciw4ea.png",
        "miniatura": "https://t00.deviantart.net/vVXRH20HBJXxFd45IZwyTkstJ5U=/300x200/filters:fixed_height(100,100):origin()/pre00/18fd/th/pre/f/2018/213/7/e/sheith_band_au_by_djune_y-dciw4ea.png"
    },
    {
        "url": "https://www.deviantart.com/vawie-art/art/Cooling-Down-757361003",
        "title": "Cooling Down",
        "category": "Animals",
        "username": "Vawie-Art",
        "usericon": "https://a.deviantart.net/avatars/v/a/vawie-art.jpg?8",
        "img": "https://orig00.deviantart.net/a68d/f/2018/214/5/a/cooling_down_by_vawie_art-dciwvhn.jpg",
        "miniatura": "https://t00.deviantart.net/f3t7hNFaMN1U3i-piT0itfUdszs=/300x200/filters:fixed_height(100,100):origin()/pre00/5b76/th/pre/f/2018/214/5/a/cooling_down_by_vawie_art-dciwvhn.jpg"
    },
    {
        "url": "https://www.deviantart.com/vegacolors/art/Sunset-Run-762706551",
        "title": "Sunset Run",
        "category": "Games",
        "username": "VegaColors",
        "usericon": "",
        "img": "https://orig00.deviantart.net/2966/f/2018/248/e/1/sunset_run_by_vegacolors-dcm3g53.jpg",
        "miniatura": "https://t00.deviantart.net/-5SpzOBTn4fcvPRoWhhfMWGOIkA=/300x200/filters:fixed_height(100,100):origin()/pre00/caac/th/pre/f/2018/248/e/1/sunset_run_by_vegacolors-dcm3g53.jpg"
    },
    {
        "url": "https://www.deviantart.com/leonidafremov/art/Autumn-Stream-by-Leonid-Afremov-762576763",
        "title": "Autumn Stream by Leonid Afremov",
        "category": "Landscapes & Scenery",
        "username": "Leonidafremov",
        "usericon": "",
        "img": "https://orig00.deviantart.net/6c9b/f/2018/247/b/c/autumn_stream_by_leonidafremov-dcm0nzv.jpg",
        "miniatura": "https://t00.deviantart.net/qsQuUJygTOG5T3AxYpt665kdpSc=/300x200/filters:fixed_height(100,100):origin()/pre00/2449/th/pre/f/2018/247/b/c/autumn_stream_by_leonidafremov-dcm0nzv.jpg"
    },
    {
        "url": "https://www.deviantart.com/erik-m1999/art/The-costs-of-war-can-never-be-truly-accounted-for-762724196",
        "title": "The costs of war can never be truly accounted for",
        "category": "Male",
        "username": "Erik-M1999",
        "usericon": "",
        "img": "https://img00.deviantart.net/be0b/i/2018/248/b/5/the_costs_of_war_can_never_be_truly_accounted_for_by_erik_m1999-dcm3tr8.jpg",
        "miniatura": "https://t00.deviantart.net/1PM4w9dMwmxBRHyCxfw8TYeDu-c=/300x200/filters:fixed_height(100,100):origin()/pre00/cdb8/th/pre/i/2018/248/b/5/the_costs_of_war_can_never_be_truly_accounted_for_by_erik_m1999-dcm3tr8.jpg"
    },
    {
        "url": "https://www.deviantart.com/chateaugrief/art/San-Luis-Rey-Pepper-Tree-762738850",
        "title": "San Luis Rey Pepper Tree",
        "category": "Landscapes & Scenery",
        "username": "chateaugrief",
        "usericon": "",
        "img": "https://img00.deviantart.net/cca7/i/2018/248/4/5/san_luis_rey_pepper_tree_by_chateaugrief-dcm452a.jpg",
        "miniatura": "https://t00.deviantart.net/R0gmJ0i1OFLfVjMp1iO2Hom_0wc=/300x200/filters:fixed_height(100,100):origin()/pre00/fdd7/th/pre/i/2018/248/4/5/san_luis_rey_pepper_tree_by_chateaugrief-dcm452a.jpg"
    },
    {
        "url": "https://www.deviantart.com/dishwasher1910/art/Beach-discovery-762713349",
        "title": "Bech discovery",
        "category": "Other",
        "username": "dishwasher1910",
        "usericon": "",
        "img": "https://orig00.deviantart.net/6cac/f/2018/248/c/0/rnp_sd_by_dishwasher1910-dcm3ldx.png",
        "miniatura": "https://t00.deviantart.net/M9-WvYnkKhHjylPR8uk9Vzqiado=/300x200/filters:fixed_height(100,100):origin()/pre00/61ca/th/pre/f/2018/248/c/0/rnp_sd_by_dishwasher1910-dcm3ldx.png"
    },
    {
        "url": "https://www.deviantart.com/pajunen/art/Strange-Dreams-762723351",
        "title": "Strange Dreams",
        "category": "Surreal",
        "username": "Pajunen",
        "usericon": "",
        "img": "https://orig00.deviantart.net/1f57/f/2018/248/3/9/strange_dreams_by_pajunen-dcm3t3r.jpg",
        "miniatura": "https://t00.deviantart.net/PlCh1gvA0oyDTIwhlq6D-1Y1Fys=/300x200/filters:fixed_height(100,100):origin()/pre00/7670/th/pre/f/2018/248/3/9/strange_dreams_by_pajunen-dcm3t3r.jpg"
    },
    {
        "url": "https://www.deviantart.com/teranen/art/Autumn-Ych-762694116",
        "title": "Autumn Ych",
        "category": "Animals",
        "username": "teranen",
        "usericon": "",
        "img": "https://img00.deviantart.net/6039/i/2018/248/1/8/autumn_ych_by_teranen-dcm36jo.png",
        "miniatura": "https://t00.deviantart.net/d9M9k_tDTn_ZQq9cR9UVMKtNg6Q=/300x200/filters:fixed_height(100,100):origin()/pre00/aa3d/th/pre/i/2018/248/1/8/autumn_ych_by_teranen-dcm36jo.png"
    },
    {
        "url": "https://www.deviantart.com/ellysiumn/art/Arctic-Solitude-762261383",
        "title": "Arctic Solitude",
        "category": "Abstract",
        "username": "Ellysiumn",
        "usericon": "",
        "img": "https://img00.deviantart.net/20a8/i/2018/248/2/1/arctic_solitude_by_ellysiumn-dcltwnb.jpg",
        "miniatura": "https://t00.deviantart.net/P0A6PZVGcFaVfgaBkqZ0ZZmguBM=/300x200/filters:fixed_height(100,100):origin()/pre00/1bd0/th/pre/i/2018/248/2/1/arctic_solitude_by_ellysiumn-dcltwnb.jpg"
    },
    {
        "url": "https://www.deviantart.com/ellysiumn/art/Magical-red-moon-762170262",
        "title": "Magical red moon",
        "category": "Abstract",
        "username": "Ellysiumn",
        "usericon": "",
        "img": "https://img00.deviantart.net/a670/i/2018/245/3/f/magical_red_moon_by_ellysiumn-dclryc6.jpg",
        "miniatura": "https://t00.deviantart.net/wyQVlJMpxkoM4FGhJI0fp6JISEY=/300x200/filters:fixed_height(100,100):origin()/pre00/58b5/th/pre/i/2018/245/3/f/magical_red_moon_by_ellysiumn-dclryc6.jpg"
    }
];